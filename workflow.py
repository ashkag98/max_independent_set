# Sketch about Workflow of the Software

# IMPORTS
import yaml
import os
import sys
import pandas as pd
import numpy as np
import networkx as nx
from networkx.algorithms.approximation import independent_set, vertex_cover
import igraph as ig
import matplotlib.pyplot as plt
from matplotlib import cm
from mip import *
import re
from tqdm import tqdm, trange
import itertools

# 1. Checke, ob taken indices schon kollidieren
# 2. Falls ja, werfe index raus oder nur warning
# 3. Baue Matrix
# 4. Solve ILP
# 5. Return Ergebnis


# ---------------------------- INFO FROM FRONTEND ----------------------------
# input: yaml file (als Argument)
# output: dictionary d
# Dictonary als Datenstrukur
# enthält Information über markierte Indices und die zu betrachtenen Indices


# -------------------------- HILFSFUNKTIONEN
# Für die Datenstruktur
# 1. Um bis unterste Ebene zu iterieren: recursive_items()
# input: dictionary
# output: (key,) value

# ------ GET INDICES
# input: Dictionary d
# output:
#    forward_reverse: liste mit allen indices als (forward, reverse) tupel
#    selected: liste aus dictionaries aus den indices und den jeweilgen Samplenamen
#    index_for_matrix: liste, die sowohl dictonaries als auch tupel enthält
#   diejenige liste, die später für die Matrix eingelesen wird


# -------------------------- DIFFERENCE COEFFICIENT
# Editdistance (Levinthstein Distanz - minimale Editdistanz)
# input: sequenz a, b
# 1. Fall: Distanz zwischen zwei Strings
# 2. Fall: Distanz zwischen zwei Tupel
# 3. Fall: Distanz zwischen einem Dictonary und einem Tupel
# output: Distanz zwischen beiden Sequenzen (integer)


# -------------------------- MATRIX
# nachdem die matrix erstellt wurde
# input: indexsequenzen als liste
# output: array matrix in n*n, wobei n die anzahl der indices in der liste entsprechen

# -------------------------- Distanzthreshold (konfigurierbar)
# Maximale Distanz paarweiser indexsequenzen

# -------------------------- GRAPH (optional)
# entweder aus NetworkX oder IGraph
# input: Matrix als pandas dataframe
# output: Graph (,der cluster bei vielen knoten cluster zeigt)


# -------------------------- markierte Indices
# Liste aus Indices aus einem oder mehreren Kits, die bereits prepariert wurden


# # -------------------------- Maximal Independent Set using NetworkX
# input: graph aus selbigem modul
# output: liste aus knoten, die set aus potentiellen indices darstellt


# # -------------------------- Maximal Independent Set using igraph
# input: graph aus selbigem modul, marktierte indices, die zu beachten sind
# output: set aus indices, die mindestens markierte indices enthalten


# # -------------------------- Optimizing using Gurobi
# Voraussetzung: markierte Indices und maximale Anzahl an Indices pro Set
# input: variables
# output: optimal solution


# # -------------------------- Benchmark
# comparison between execution before and after using gurobi
# input: heuristic model and ilp model
# output: cpu time
