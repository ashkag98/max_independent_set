# max_independent_set

Minimum-distance constrained selection of Illumina index combinations using Independent Sets

# Strain Typing - a Snakemake pipeline for executing different strain typing tools
Development of a Snakemake pipeline for benchmarking and routine execution of strain detection software

## Description

This Pipeline will do a Strain typing for different microbes and identify metagenomic reads on strain-level based on different tools. These tools were analysed and compared based on different statistical parameters. 
It works with Snakemake and includes several strain typing tools:

- strainest 
- sparse
- metamlst
- sigma

We worked with metagenomic samples in .fastq format and processes a mapping and sorting step for each tool. As result we get the procentage abundance of strains. This abundance dependes on the similarity of the database we used. Every tool uses its own way to deal with the database.

## Installation

```bash
cd /path/to/installation 
git clone git@gitlab.com:bfr_bioinformatics/strain_typing_ashkan.git with SSH
```
or 
```bash
cd /path/to/installation
git clone https://gitlab.com/bfr_bioinformatics/strain_typing_ashkan.git with Html
```

## Configuration

To run the Pipeline on Snakemake conda is needed. If you dont familiar with conda yet go through the installation-guide [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)

To avoid long waiting processes and clashing tool-versions *mamba* is recommended:
```bash
conda install mamba
```

After that, all the conda commands can substitute with mamba.

If conda is already installed initialize a conda environment containing Snakemake:
```bash
mamba create -c conda-forge --name strain_typing snakemake
```

For the ideal handling and executing of tools, i recommend you to generate a environment for each of the yaml files: 
```bash
mamba create -f environment.yaml
```

## Tools

### strainest
reference based method, use SNV Profiles to determine the number and identity of coexisting strains and their relative
abundances in mixed metagenomic samples.
ability to disentangle complex communities of closely related strains.

full documentation on [strainest](https://github.com/compmetagen/strainest)
Example with Ecoli samples and the NCBI database

Usage:

```bash
strainest mapgenomes genomes ref_genom output
strainest map2snp ref_genom MR SNP
strainest snpdist SNP SNP_dist
strainest snpclust SNP SNP_dist SNP_clust cluster
strainest est SNP_clust sorted_reads Folder
```

Output:
- abund.txt: the predicted abundances for each reference genome
- max_ident.txt: for each ref_genom, gives percentage of allels that are present in the metagenom
- info.txt: information about the prediction, including the prediction Pearson R
- counts.txt: number of counts for each SNV position/base pairs
- mse.pdf: Lasso cross-validation plot as a function of the shrinkage coefficient.


### sparse
index all genomes in large reference databases such as RefSeq into hierarchical clusters based on different sequence identity thresholds

full documentation on [sparse](https://github.com/zheminzhou/SPARSE/)

For this Project we used full genomes of Ecoli get from the RefSeq Database (assembly_summary.txt)

Usage:

map and evaluate all reads in both fastq-files against the specified mapping databases:
```bash
SPARSE.py predict --dbname <refseq_5> --mapDB <supopulation,representetive> --workspace /path/to/workspace --r1 <read1> --r2 <read2> -t <threads>
```
Provide a report that combines multiple ‘sparse predict’ runs together into a tab-delimited text file:
(This command also identifies potential pathogens in the predictions.)
```bash
SPARSE.py report /path/to/workspace > report.txt
```

Output:
- profile.txt: tab-delimited file that consist of ANI-Level, taxonomic order and genom index

For my Pipeline and the later Benchmarking, we get the Information of the profile.txt. For more knowledge about the taxonomic origin of the species you can extract the reads and align them.  

### metamlst
performs an in-silico Multi Locus Sequence Typing (MLST) Analysis on metagenomic samples for
Strain level identification and Microbial population analysis

full documentation on [metamlst](https://github.com/SegataLab/metamlst) 

Usage:
Again, first build a index:
```bash
bowtie2 -1 <read1> -2 <read2> --threads <threads> --very-sensitive-local -a --no-unal -x <reference> | samtools view -bS - > <reads.bam>
```

Reconstruct the MLST loci from a BAMFILE aligned to the reference MLST loci:
```bash
metamlst.py <reads.bam> > /path/to/Folder
```

Output:
- out.txt: reconstructed mlst loci from alignment to the reference mlst loci
- a list of detected MLST-trackable microbial species
- a tab-separated file containing the typings of each sample provided. One file for each species
- a tab-separated file containing the updated typing table (i.e. known and newly identified Sequence Types).
- a FASTA or CSV file containing the sequences of the MLST-reconstructed loci for each sample.

### sigma
Get full documentation on [strainest](http://sigma.omicsbio.org/).

Sigma works with a config file, where it gets all its usefull information like database and sample. This config file requires a hierarchical structure of the database:

Preprocessing:
```bash:
for file in datasets/bowtie2/ref2/Escherichia_Coli/MR_genom/Escherichia_coli/*_genomic.fna; do
name=`basename $file .fna`
mkdir -p datasets/bowtie2/ref2/Escherichia_Coli/Sigma/db/$name
cp $file datasets/bowtie2/ref2/Escherichia_Coli/Sigma/db/$name
done
sigma_db= datasets/bowtie2/ref2/Escherichia_Coli/Sigma/db
```

Usage:
First, build the indices of the Database:
```bash
sigma-index-genomes -p 8 -c /path/to/sigma_config.cfg -w /path/to/workdir
```
The Output will be stored in the db folder. 

Second, the indices will be aligned against the metagenomic samples:
```bash 
sigma-align-reads -p 8 -c /path/to/sigma_config.cfg -w /path/to/workdir
```

and last but not least, sigma will compute the gvector matrix:
```bash
cd /path/to/workdir; sigma -t <multiprocess>
```

or optional if you interested in sigma_out.qmatrix.txt:
```bash
sigma -i <q_matrix> -t -c /path/to/sigma_config.cfg -w /path/to/workdir
```
## Benchmarking 
The Output files were compared and relevant parameters filtered. Depends on these parameters we aggregate the values in a Table. 
The relative abundance of a strain that was detected from a tool in a accesion is the parameter we choose.
Also it shows how many strains were detected
Furthermore, we visualised our final Table in a heat map using seaborn.

## Results
In the process of implementing the pipeline, i perceive three important factors dealing with the tools: usability, handling and power.
Its shows that all the tools were simalar in the way of usability but most difference showed in handling and power.
StrainEst was easy to handle, but diffult to use cause the preprocessing steps takes the most. As the output of my choice i use the abund.txt. Was not so powerfull but clear in information.
Sparse was more diffult in handle and use. Not so sure about the power jet.
Sigma was easy in usage in the way that the commands were pretty similar. But unfortunatly there are missing files for further statistical analysis. Powerfull, cause more information that other tools.
The Output we considered were similar for StrainEst and Sigma. 

The Results of metamlst are taken out of consideration, because the computed snv matrix were on low throughput and not necessarily relevant for our later usage. 


## Authors and acknowledgment

Feel free to consult me for questions or feedback:

Ashkan Ghassemi @ashkag98 <Ashkan.Ghassemi(at)bfr.bund.de>
