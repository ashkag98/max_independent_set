# -----------------------------------
# Load Index Adapter from workbook
# input: worksheet
# output: dataframes of sequences
worksheet = pd.ExcelFile("dataset/Index_adapters.xlsx")
kits = {sheet: worksheet.parse(sheet, header=None, skiprows=1) for sheet in worksheet.sheet_names}
nextera = kits['IDT_Nextera'].iloc[1:].reset_index(drop=True).drop([96, 193, 290])
truseq_s = kits['TruSeq_Single']
truseq_d = kits['TruSeq_double'].drop([12, 13]).drop(2, axis=1)
truseq = kits['IDT_TruSeq']

# -----------------------------------
# Extract i7 Index Sequences
# input: dataframes of sequences
# output: list of sequences
nextera_seq = nextera[2].tolist()  # 384 - list of strings
tru_s_seq = truseq_s[1].tolist()  # 24
tru_d_seq = truseq_d[1].tolist()  # 20
tru_seq = truseq[2].tolist()  # 96

tuple_seqs = [nextera_seq, tru_s_seq, tru_d_seq, tru_seq]

# pairwise combinations of two seqlists
# input: list of sequences
# output: list of lists of sequences
combseq = list(itertools.combinations(tuple_seqs, 2))  # list of tuples of lists

pairedseqs = []
for (l1, l2) in combseq:
    pairedseqs.append([*l1, *l2])

