def milp(desired_samples, sorted_indices, diff_matrix, dictionary, outfile):
    """Mixed Integer Linear Programming Approach
        Args:
            desired_samples(dict): Kits und their amount of desired sample names
            sorted_indices(list): All Indices sorted by Kits from sort_index()
            diff_matrix(pandas.DataFrame): levenshtein distance matrix with row and column names
            dictionary(dict): Dictionary D from load_yaml()
            outfile(str): Define Output File in mlp format with each variable and constraint that will be used
        Returns:
            list: Optimal Variables as Strings
            str: Description about the fit of the model based on the input
            int: Sum of the desired sample names
        Notes:
            Whether there are collisions between taken indices, the weight of each index will be changed
    """
    print('Step 5: building model to optimize the constrained-based selection of index combinations')
    # Define Model
    m = Model(sense=MAXIMIZE, solver_name=CBC)

    sum_of_desired = 0
    for key, value in desired_samples.items():
        sum_of_desired += value

    # Decision Variables
    n = len(sorted_indices)
    taken_indices = [i[1] for i in sorted_indices if i[2] is not None]
    l = len(taken_indices) + sum_of_desired
    x = [m.add_var(var_type=BINARY) for i in range(n)]
    weight = 1

    # Occurrence of Nucleotide in Barcode set n
    padded_indices = index_evenlength(sorted_indices)
    pad_indices_for = [forward[1][0] for forward in padded_indices]
    max_length = len(pad_indices_for[0])
    proxy_a = [m.add_var(var_type=BINARY) for i in range(max_length)]
    proxy_c = [m.add_var(var_type=BINARY) for i in range(max_length)]
    proxy_g = [m.add_var(var_type=BINARY) for i in range(max_length)]
    proxy_t = [m.add_var(var_type=BINARY) for i in range(max_length)]
    nla = [m.add_var() for i in range(max_length)]
    nlc = [m.add_var() for i in range(max_length)]
    nlg = [m.add_var() for i in range(max_length)]
    nlt = [m.add_var() for i in range(max_length)]

    # Constraints
    # 1) Pairwise comparison between the indices. Out of collide indices will be one index selected
    for i in range(len(sorted_indices)):
        for j in range(i + 1, len(sorted_indices)):
            if diff_matrix.iloc[i][j] == 0:
                m += x[i] + x[j] <= 1

    # 2) Marked Indices will be fixed. If they collide a weight will be defined
    if not taken_collision_check(sorted_indices, dictionary):
        for idx, i in enumerate(sorted_indices):
            if i[2] is not None:
                m += x[idx] == 1
    else:
        # define weights if taken_collision_check = True
        weight = [(sum_of_desired + 1) if i[2] is not None else 1 for idx, i in enumerate(sorted_indices)]

    # 3) number of selected indices is equal to desired sample names size
    for key, value in desired_samples.items():
        tmp = [idx for idx, j in enumerate(sorted_indices) if j[0] == key if j[2] is None]
        m += xsum(x[i] for i in tmp) == value

    # 4) required nucleotide balance in the optimal set

    for position in range(max_length):
        m += -nla[position] + l / 4 <= proxy_a[position]
        m += nla[position] - l / 4 <= proxy_a[position]
        m += -nlc[position] + l / 4 <= proxy_c[position]
        m += nlc[position] - l / 4 <= proxy_c[position]
        m += -nlg[position] + l / 4 <= proxy_g[position]
        m += nlg[position] - l / 4 <= proxy_g[position]
        m += -nlt[position] + l / 4 <= proxy_t[position]
        m += nlt[position] - l / 4 <= proxy_t[position]

        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'A']) == nla[position]
        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'C']) == nlc[position]
        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'G']) == nlg[position]
        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'T']) == nlt[position]

    # Objective
    if isinstance(weight, int):  # wenn es zu keiner kollision bei taken kommt, bleibt weight konstant
        # Nucleotid balance könnte auch eine Lösung geben, die nicht optimal sein muss
        m.objective = maximize(xsum(weight * x[i] for i in range(n)) -
                               xsum(proxy_a[i] + proxy_c[i] + proxy_g[i] + proxy_t[i] for i in range(max_length)))
    elif isinstance(weight, list):  # wenn es zu kollision bei taken kommt, wird weight zu einer liste mit gewichtung
        m.objective = maximize(xsum(weight[i] * x[i] for i in range(n)) -
                               xsum(proxy_a[i] + proxy_c[i] + proxy_g[i] + proxy_t[i] for i in range(max_length)))

    print('model has {} vars, {} constraints and {} nzs'.format(m.num_cols, m.num_rows, m.num_nz))

    status = m.optimize()

    m.write(f'results/model/model_{outfile}.lp')

    # print the model
    print(f"status: {m.status}")
    print(f"objective: {m.objective_value}")

    opt_var = []
    if status == OptimizationStatus.OPTIMAL:
        print('optimal solution cost {} found'.format(m.objective_value))
    elif status == OptimizationStatus.FEASIBLE:
        print('sol.cost {} found, best possible: {} '.format(m.objective_value, m.objective_bound))
    elif status == OptimizationStatus.NO_SOLUTION_FOUND:
        print('no feasible solution found, lower bound is: {} '.format(m.objective_bound))
    if status == OptimizationStatus.OPTIMAL or status == OptimizationStatus.FEASIBLE:
        print('solution:')
        for v in m.vars:
            if abs(v.x) > 1e-6:  # only printing non-zeros
                print('{} : {} '.format(v.name, v.x))
                opt_var.append(v.name)
    return opt_var, status