#!/usr/bin/env python3

import random
import matplotlib.ticker as mtick
import yaml
import os
import pandas as pd
from mip import *
import re
from sys import exit
import time
import argparse
import warnings
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure
import seaborn as sns  # create and view data
from Levenshtein import distance
import edlib
from operator import itemgetter


print("Program Starting..")
time.sleep(0.5)


def load_yaml(file):
    """read  yaml in dictionary
    Get necessary Information from Frontend in yml format

    Args:
        file(str): Yaml File
    Returns:
        dict: Dictionary D
    Raises:
        YAMLError: File missing or incorrect type
    """
    print("Step 1: Reading in the Yaml File..")
    time.sleep(0.5)
    with open(file, 'r') as stream:
        try:
            dictionary = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return dictionary


def check_path(outdir):
    """check whether desired output directory already exist
    if not the dictionary will be made

        Args:
            outdir(str): output directory
        """
    if not os.path.exists(outdir):
        os.makedirs(outdir)


def get_index(dictionary):
    """get all indices of the dictionary

    Args:
        dictionary(dict): Dictionary D
    Returns:
        list: List of all Indices
    """
    list_for_matrix = []
    for key, value in dictionary.items():
        if isinstance(value, dict):
            for key1, value1 in value['kits'].items():
                sets = key1
                for tupel, name in value1.items():
                    if "," in tupel:
                        forward_reverse = tuple(tupel.split(','))
                        for_matrix = (sets, forward_reverse, name)
                        list_for_matrix.append(for_matrix)
    return list_for_matrix


def sort_index(unsorted_index):
    """sort the index combinations

    Args:
        unsorted_index(list): List of all indices
    Returns:
        list: List of all Indices sorted by Kits
    """
    print("Step 3: Indices will be sorted depends on the Kits..")
    time.sleep(0.5)
    sorted_list = sorted(unsorted_index, key=lambda sets: sets[0])
    taken_indices_idx = [idx for idx, i in enumerate(sorted_list) if i[2] is not None]
    return sorted_list, taken_indices_idx


def get_kit(dictionary):
    """Get all informations of Kits

    Args:
        dictionary(dict): Dictionary D from load_yaml()
    Returns:
        dict:
            - kits: Sets and their size of indices
            - kits_index_to_selected: selected sets and their size of Indices
            - kits_desired_size: selected sets and their desired sample names size
            - kits_sample_names: selected Sets and their desired sample names
    """
    kits, kits_selected, kits_desired_size = ({} for dicts in range(3))
    kits_sample_names = defaultdict(list)
    list_for_matrix = get_index(dictionary)
    for for_matrix in list_for_matrix:
        if for_matrix[0] in kits:
            kits[for_matrix[0]] += 1
        else:
            kits[for_matrix[0]] = 1
        if for_matrix[2] is None:
            if for_matrix[0] in kits_selected:
                kits_selected[for_matrix[0]] += 1
            else:
                kits_selected[for_matrix[0]] = 1
    to_select = dictionary["to_select"]["kits"]
    for key, value in to_select.items():
        kits_desired_size[key] = 0
        for key1 in value["desired_samplenames"].items():
            kits_desired_size[key] += 1
    for kit, index in to_select.items():
        for samplename, leer in index["desired_samplenames"].items():
            kits_sample_names[kit].append(samplename)
    kits_sample_names = dict(kits_sample_names)
    return kits, kits_selected, kits_desired_size, kits_sample_names


def levenshtein(seq1, seq2):
    """Levenshtein Distance Coefficient for Sequence Distance

    Args:
        seq1, seq2(str): DNA Sequences
    Returns:
        int: Distance of the Sequences
    """
    size_x, size_y = len(seq1) + 1, len(seq2) + 1
    matrix = np.zeros((size_x, size_y))
    for x in range(size_x):
        matrix[x, 0] = x
    for y in range(size_y):
        matrix[0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x - 1] == seq2[y - 1] or (seq1[x - 1] == "-" or seq2[y - 1] == "-"):
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1],
                    matrix[x, y - 1] + 1)
            else:
                matrix[x, y] = min(
                    matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1] + 1,
                    matrix[x, y - 1] + 1)
    return matrix[size_x - 1, size_y - 1]


def efficient_levensthein(seq1, seq2):
    """Levenshtein Distance Coefficient for Sequence Distance

        Args:
            seq1, seq2(str): DNA Sequences
        Returns:
            int: Distance of the Sequences
        """
    if "-" in seq1 or "-" in seq2:
        effective_seq1, effective_seq2 = seq1.rstrip("-"), seq2.rstrip("-")
        if len(effective_seq1) < len(effective_seq2):
            shorter_seq2 = seq2[:len(effective_seq1)]
            difference = distance(shorter_seq2, effective_seq1)
        else:
            shorter_seq1 = seq1[:len(effective_seq2)]
            difference = distance(shorter_seq1, effective_seq2)
    else:
        difference = distance(seq1, seq2)
    return difference


#    difference = max(edlib.align(index1[0], index2[0], mode="SHW", task="path")['editDistance'],
#                     edlib.align(index1[1], index2[1], mode="SHW", task="path")['editDistance'])

def dist(index1, index2):
    """The actual distance Coefficient for Index Combinations

    Args:
        index1, index2(tuple): Index Combinations of two DNA Sequences (Tuple)
    Returns:
        int: Levenshtein Distance of two Tuples
    """
    difference = max(efficient_levensthein(index1[0], index2[0]), efficient_levensthein(index1[1], index2[1]))

    return difference


def index_padding(index_list_sorted):  # def index_padding
    """Fill Indices of Uneven Length (Padding)

        Args:
            index_list_sorted(list): list with Tuples of index combinations with uneven length
        Returns:
            list: list with Tuples of index combinations with even length
        """
    just_indices, tmp, filled = ([] for empty_list in range(3))
    for entry in index_list_sorted:
        if isinstance(entry[1], tuple):
            forward, reverse = entry[1][0], entry[1][1]
        else:
            forward, reverse = entry[0], entry[1]
        just_indices.extend([forward, reverse])
    max_length = len(max(just_indices, key=len))
    tmp = [str(seperated).ljust(max_length, '-') for seperated in just_indices]
    for i in range(len(tmp) - 1):
        temp_tuple = (tmp[i] + "," + tmp[i + 1])
        filled.append(tuple(temp_tuple.split(',')))
    filled = filled[0::2]
    return filled


# braucht ne weile FIX THIS SHIT
def distance_matrix(sequences):  # könnte auch in compute matrix stehen
    """Compute distance matrix

        Args:
            sequences(list): list of sequences with even length
        Returns:
            np.array: distance matrix as numpy array
        Notes:
            take a wile to compute if input arg is larger than 1000 sequences ~ 2 minutes
        """
    n = len(sequences)
    my_array = np.zeros((n, n), dtype=np.uint8)
    for i, ele_1 in enumerate(sequences):
        for j, ele_2 in enumerate(sequences):
            if j >= i:
                break
            difference = dist(ele_1, ele_2)  # Levenshtein Distance
            my_array[i, j] = difference
            my_array[j, i] = difference
    return my_array


def compute_matrix(filled_indices, sorted_tuple, threshold):
    """Set a threshold in difference matrix to a minimum distance constrained, so the
        pairwise base-distance is maximal the threshold value
        otherwise the cell value will be assigned 0

        Args:
            filled_indices(list): Sequences with even length for using distance_matrix()
            sorted_tuple(list): All Indices sorted by Kits from sort_index()
            threshold(int): Number of mismatches allowed between the index combinations
        Returns:
            pandas.DataFrame: distance matrix with row and column names
        Notes:
            take a wile to compute if input arg is larger than 1000 sequences ~ 6 minutes
        """
    matrix = distance_matrix(filled_indices)
    temp = [str(i) for i in sorted_tuple]
    dfs = pd.DataFrame(matrix, index=temp, columns=temp)
    dfs[dfs < threshold] = 0
    dfs[dfs > 0] = 1
    return dfs


def taken_collision_check(sorted_list, dictionary):
    """Check for collisions between taken indices (value in different matrix = 0)
        Args:
            sorted_list(list): All Indices sorted by Kits from sort_index()
            dictionary(dict): Dictionary D from load_yaml()
        Returns:
            bool: Description about Collision between taken indices
        Notes:
            Taken indices are "marked" and should collide between each other. After this function detected
            collisions after MILP the program will throw an warning, the program will run further.
            If the program should crash after collisions are occured the -break flag should be used.
    """
    collsion = False
    taken_indices = [i for i in sorted_list if i[2] is not None]
    index_kombi = []
    taken_filled = index_padding(taken_indices)
    threshold = dictionary["dist_threshold"]
    matrix_taken = compute_matrix(taken_filled, taken_indices, threshold)
    for i in range(len(taken_filled)):
        for j in range(i + 1, len(taken_filled)):
            if matrix_taken.iloc[i][j] == 0:
                index_kombi.append((i, j))
                collsion = True
    return collsion, matrix_taken, index_kombi


def create_content_matrix(filled_list):  # muss nicht mehr gepaddet werden
    """Position Frequency Matrix for every forward indices for the nucleotide balance constraint in MILP
        Args:
            filled_list(list): Sequences with even length from index_padding()
        Returns:
            pandas.DataFrame: A, C, G, T Content
    """
    indices_for = [forward[0] for forward in filled_list]
    n = len(indices_for[0])
    frequency_matrix = {base: np.zeros(n, dtype=np.int8)
                        for base in 'ACGT'}
    for dna in indices_for:
        for index, base in enumerate(dna):
            if base in 'ACGT':
                frequency_matrix[base][index] += 1
    df_base = pd.DataFrame.from_dict(frequency_matrix)
    df_base['sum'] = df_base.sum(axis=1)

    df_base['A_Content'], df_base['C_Content'], df_base['G_Content'], df_base['T_Content'] = \
        df_base['A'] / df_base['sum'], \
        df_base['C'] / df_base['sum'], \
        df_base['G'] / df_base['sum'], \
        df_base['T'] / df_base['sum']
    df_base = df_base.drop(['A', 'C', 'G', 'T', 'sum'], axis=1).T
    return df_base


def plot_nucleotide_matrix(optimal_variables, outfile):
    """Visualization of Position Frequency Matrix and the nucleotide balance for every barcode position
        Args:
            optimal_variables(list): Optimal Barcode Set from MILP model contains taken indices and desired samples
            outfile(str): Outfile name in .png Format
        Returns:
            None
    """
    random.shuffle(optimal_variables)
    optimal_variables = index_padding(optimal_variables)
    intervall = []
    opt_length = len(optimal_variables)
    for i in range(4):
        tmp_bases = optimal_variables[:int(opt_length)]
        intervall.append(tmp_bases)
        opt_length /= 2

    outdir_content = f"results/{outfile}/base_content/"
    check_path(outdir_content)
    print(f"Nucleotide Balance Distribution will be visualized and stored in {outdir_content}{outfile}.png")
    time.sleep(0.5)
    base_matrices = [create_content_matrix(seq_list) for seq_list in intervall]
    for i in intervall:
        for matrix in base_matrices:
            matrix['total'] = pd.Series([0.25, 0.25, 0.25, 0.25], index=matrix.index)
        print(
            f"To check if base contents are equally distributed for each index position with length {len(i)}: "
            "\n", matrix)

    plt.rcParams['figure.dpi'] = 360
    fig, axes = plt.subplots(figsize=(12, 8), nrows=2, ncols=2)
    xlabels = list(base_matrices[0].columns)
    coords = [(1, 1), (1, 0), (0, 1), (0, 0)]

    base_matrices[0].T.plot(ax=axes[1, 1], kind='bar', stacked=True)
    axes[1, 1].set_xlabel(f"{len(intervall[0])} Barcodes", size=12)
    axes[1, 1].set_xticklabels(xlabels, rotation=0)
    axes[1, 1].set_ylabel("nucleotide %", size=12)
    axes[1, 1].yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    axes[1, 1].legend([], [], frameon=False)

    axes[1, 1].axhline(y=0.25, color='gray', linestyle='--')
    axes[1, 1].axhline(y=0.5, color='gray', linestyle='--')
    axes[1, 1].axhline(y=0.75, color='gray', linestyle='--')
    axes[1, 1].axhline(y=1, color='gray', linestyle='--')

    base_matrices[1].T.plot(ax=axes[1, 0], kind='bar', stacked=True)
    axes[1, 0].set_xlabel(f"{len(intervall[1])} Barcodes", size=12)
    axes[1, 0].set_xticklabels(xlabels, rotation=0)
    axes[1, 0].set_ylabel("nucleotide %", size=12)
    axes[1, 0].yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    axes[1, 0].legend([], [], frameon=False)

    axes[1, 0].axhline(y=0.25, color='gray', linestyle='--')
    axes[1, 0].axhline(y=0.5, color='gray', linestyle='--')
    axes[1, 0].axhline(y=0.75, color='gray', linestyle='--')
    axes[1, 0].axhline(y=1, color='gray', linestyle='--')

    base_matrices[2].T.plot(ax=axes[0, 1], kind='bar', stacked=True)
    axes[0, 1].set_xlabel(f"{len(intervall[2])} Barcodes", size=12)
    axes[0, 1].set_xticklabels(xlabels, rotation=0)
    axes[0, 1].set_ylabel("nucleotide %", size=12)
    axes[0, 1].yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    axes[0, 1].legend([], [], frameon=False)

    axes[0, 1].axhline(y=0.25, color='gray', linestyle='--')
    axes[0, 1].axhline(y=0.5, color='gray', linestyle='--')
    axes[0, 1].axhline(y=0.75, color='gray', linestyle='--')
    axes[0, 1].axhline(y=1, color='gray', linestyle='--')

    base_matrices[3].T.plot(ax=axes[0, 0], kind='bar', stacked=True)
    axes[0, 0].set_xlabel(f"{len(intervall[3])} Barcodes", size=12)
    axes[0, 0].set_xticklabels(xlabels, rotation=0)
    axes[0, 0].set_ylabel("nucleotide %", size=12)
    axes[0, 0].yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    axes[0, 0].legend([], [], frameon=False)

    axes[0, 0].axhline(y=0.25, color='gray', linestyle='--')
    axes[0, 0].axhline(y=0.5, color='gray', linestyle='--')
    axes[0, 0].axhline(y=0.75, color='gray', linestyle='--')
    axes[0, 0].axhline(y=1, color='gray', linestyle='--')

    sns.despine()

    leg = fig.legend(labels=['A', 'C', 'G', 'T'], loc="center right", bbox_to_anchor=(1, 0.5), title="Nucleotide Base")
    leg.legendHandles[0].set_color('red')
    leg.legendHandles[1].set_color('green')
    leg.legendHandles[2].set_color('orange')
    leg.legendHandles[3].set_color('blue')

    plt.subplots_adjust(right=0.75)

    plt.tight_layout()
    plt.savefig(f'{outdir_content}{outfile}.png')


def milp(desired_samples, sorted_indices, diff_matrix, dictionary, outfile):
    """Mixed Integer Linear Programming Approach
        Args:
            desired_samples(dict): Kits und their amount of desired sample names
            sorted_indices(list): All Indices sorted by Kits from sort_index()
            diff_matrix(pandas.DataFrame): levenshtein distance matrix with row and column names
            dictionary(dict): Dictionary D from load_yaml()
            outfile(str): Define Output File in mlp format with each variable and constraint that will be used
        Returns:
            list: Optimal Variables as Strings
            str: Description about the fit of the model based on the input
            int: Sum of the desired sample names
        Notes:
            Whether there are collisions between taken indices, the weight of each index will be changed
    """
    print('Step 5: building model to optimize the constrained-based selection of index combinations')

    # Define Model
    m = Model(sense=MAXIMIZE, solver_name=CBC)

    sum_of_desired = 0
    for key, value in desired_samples.items():
        sum_of_desired += value

    # Decision Variables
    n = len(sorted_indices)
    taken_indices = [i[1] for i in sorted_indices if i[2] is not None]
    optimal_set_len = len(taken_indices) + sum_of_desired
    x = [m.add_var(var_type=BINARY) for i in range(n)]

    # Occurrence of Nucleotide in Barcode set n
    padded_indices = index_padding(sorted_indices)
    pad_indices_for = [forward[1] for forward in padded_indices]
    max_length = len(pad_indices_for[0])

    proxy_a = [m.add_var() for i in range(max_length)]
    proxy_c = [m.add_var() for i in range(max_length)]
    proxy_g = [m.add_var() for i in range(max_length)]
    proxy_t = [m.add_var() for i in range(max_length)]

    nla = [m.add_var() for i in range(max_length)]
    nlc = [m.add_var() for i in range(max_length)]
    nlg = [m.add_var() for i in range(max_length)]
    nlt = [m.add_var() for i in range(max_length)]

    # Constraints
    # 1) Pairwise comparison between the indices. Out of collide indices will be one index selected
    for i in range(len(sorted_indices)):
        for j in range(i + 1, len(sorted_indices)):
            if diff_matrix.iloc[i][j] == 0:
                m += x[i] + x[j] <= 1

    # 2) number of selected indices is equal to desired sample names size
    for key, value in desired_samples.items():
        tmp = [idx for idx, j in enumerate(sorted_indices) if j[0] == key if j[2] is None]
        m += xsum(x[i] for i in tmp) <= value


    # 3) required nucleotide balance in the optimal set - cost function
    for position in range(max_length):
        m += -nla[position] + optimal_set_len / 4 <= proxy_a[position]
        m += nla[position] - optimal_set_len / 4 <= proxy_a[position]
        m += -nlc[position] + optimal_set_len / 4 <= proxy_c[position]
        m += nlc[position] - optimal_set_len / 4 <= proxy_c[position]
        m += -nlg[position] + optimal_set_len / 4 <= proxy_g[position]
        m += nlg[position] - optimal_set_len / 4 <= proxy_g[position]
        m += -nlt[position] + optimal_set_len / 4 <= proxy_t[position]
        m += nlt[position] - optimal_set_len / 4 <= proxy_t[position]

        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'A']) == nla[
            position]
        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'C']) == nlc[
            position]
        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'G']) == nlg[
            position]
        m += xsum([x[idx] for idx, barcode in enumerate(pad_indices_for) if barcode[position] == 'T']) == nlt[
            position]

    # weights
    # weight_taken = optimal_set_len * weight_balance
    weight_balance = max_length * len(pad_indices_for) * 0.75

    # Objective
    # Nucleotide balance could also provide a solution that does not have to be optimal
    tmp_untaken = [idx for idx, j in enumerate(sorted_indices) if j[2] is None]
    tmp_taken = [idx for idx, j in enumerate(sorted_indices) if j[2] is not None]
    m.objective = maximize(xsum(optimal_set_len * weight_balance * x[i] for i in tmp_taken)
                           + xsum(weight_balance * x[i] for i in tmp_untaken) -
                           xsum(proxy_a[i] + proxy_c[i] + proxy_g[i] + proxy_t[i] for i in range(max_length)))

    status = m.optimize()

    m.write(f'results/{outfile}/model/{outfile}.lp')

    # print the model
    print(f"status: {m.status}")
    print(f"objective: {m.objective_value}")

    opt_var = []
    if status == OptimizationStatus.OPTIMAL:
        print('optimal solution cost {} found'.format(m.objective_value))
    elif status == OptimizationStatus.FEASIBLE:
        print('sol.cost {} found, best possible: {} '.format(m.objective_value, m.objective_bound))
    elif status == OptimizationStatus.NO_SOLUTION_FOUND:
        print('no feasible solution found, lower bound is: {} '.format(m.objective_bound))
    if status == OptimizationStatus.OPTIMAL or status == OptimizationStatus.FEASIBLE:
        print('solution:')
        for v in m.vars:
            if abs(v.x) > 1e-6:  # only printing non-zeros
                print('{} : {} '.format(v.name, v.x))
                if v.x == 1.0:
                    opt_var.append(v.name)
    return optimal_set_len, opt_var, status


def create_outyaml(optimal_variables, dictionary, sorted_indices, sample_names):
    """Create Output Yaml if optimal variables exists for frontend
            Args:
                optimal_variables(list): Optimal Variables from MILP model
                dictionary(dict): Dictionary D from load_yaml()
                sorted_indices(list): All Indices sorted by Kits from sort_index()
                sample_names(dict): Information about Kits and their size of desired sample names
            Returns:
                dict: Taken Indices + Optimal Variables from MILP model
                str: File Path of the result in Yaml format
        """
    print(
        f'A optimal solution was found. {sample_names} will be added to marked indices..')
    time.sleep(0.5)
    # -----------------------------------
    print('Optimal Variables will be hashed..')
    index_list = []
    for i in optimal_variables:
        digit = int(re.search(r'\d+', i).group())
        index_list.append(digit)
    print("Index Liste: ", index_list, " mit der Länge: ", len(index_list))

    result = {'taken_indices': {'kits': {}}, 'selected': {'kits': {}}}
    marked_indices = dictionary["taken_indices"]
    result["taken_indices"] = marked_indices
    decided = []

    kits_indexvariables = defaultdict(list)
    for idx, tupel in enumerate(sorted_indices):
        for variable in index_list:
            if idx == variable:
                decided.append(tupel[1])
                if tupel[2] is None:
                    for key, value in sample_names.items():
                        if tupel[0] == key:
                            for_rev = ','.join(tupel[1])
                            kits_indexvariables[key].append(for_rev)
    kits_indexvariables = dict(kits_indexvariables)
    print("Diese Indizes werden markiert: ", '\n' , kits_indexvariables)

    for kit, variable in kits_indexvariables.items():
        result["selected"]["kits"][kit] = dict(zip(variable, sample_names[kit]))
    return result, decided


# entry point
def main():
    # Container to hold the arguments
    parser = argparse.ArgumentParser(description='Constraint base selection of index combinations')
    parser.add_argument('-i', '--infile', metavar='', required=True, help='choose a yaml file from '
                                                                          'dataset/input directory')
    parser.add_argument('-o', '--output', metavar='', help='If not set outfile will be named after input file')
    parser.add_argument('-w', '--warnings', action='store_true', help='Run with output warnings '
                                                                      '1) File contains more than 700 Indices \n'
                                                                      '2) There are collisions between taken Indices')
    parser.add_argument('-t', '--runtime', action='store_true', help='Run with time measurement (CPU)')
    parser.add_argument('-b', '--collision_break', action='store_true', help='Break the execution after collision '
                                                                             'between taken indices')
    parser.add_argument('-rb', '--runtime_break', action='store_true', help='Break the execution if input '
                                                                            'file is too large')
    args = parser.parse_args()

    # Step1: Load the Input Yaml File
    if args.runtime:
        start_time = time.time()
    infile = args.infile
    d = load_yaml(infile)  # dictionary with all input information
    if not args.output:
        outfile = os.path.basename(infile).split(".")[0]  # input file name for output files
    else:
        outfile = args.output

    # Step2: list of all indices in dictionary
    print("Step 2: Indices will be stored..")
    time.sleep(0.5)
    list_for_matrix = get_index(d)

    list_length = len(list_for_matrix)
    if args.runtime_break:
        if list_length > 1000:
            exit("Input File is too large.."
                 "To provide efficient index selection use less indices..")

    if list_length == 1000:
        if args.warnings:
            warnings.warn("File contains many Indices, selection of combination takes a few seconds..")
            time.sleep(0.5)

    # Step 3: List will be sorted after Kits
    sorted_index_list, taken_indices_idx = sort_index(list_for_matrix)
    print(
        f"Input Yaml File {outfile} contains {list_length} with {len(taken_indices_idx)} "
        f"marked indices..")
    time.sleep(0.5)

    kits, kits_index_to_selected, kits_desired_size, kits_sample_names = get_kit(d)

    # Preprocessing before computing matrix
    print("Indices will be padded for computing the distances..")
    time.sleep(0.5)
    filled_indices = index_padding(sorted_index_list)

    # Step 4: computing the distance matrix based on levensthein distance coefficient
    outdir_matrix = f"results/{outfile}/matrix/"
    check_path(outdir_matrix)

    threshold = d["dist_threshold"]
    print("Step 4: Distance Matrix will be computed..")  # progressbar necessary?
    time.sleep(0.25)
    print('..and the threshold will be checked..')
    matrix = compute_matrix(filled_indices, sorted_index_list, threshold)
    matrix.to_csv(f"results/{outfile}/matrix/matrix_{outfile}.csv")

    collison, taken_matrix, collided_idxs = taken_collision_check(sorted_index_list, d)
    collided_combis = [(itemgetter(i[0], i[1])(sorted_index_list)) for i in collided_idxs]

    if args.collision_break:
        print("Taken indices will be checked for possible collisions..")
        time.sleep(0.5)
        if collison:
            exit("Taken Indices collide with each other.. To provide this please use other indices..")
            print(f'following Indexcombinations collide together: {collided_combis}')

    # Step 5: building model to optimize the constrained-based selection of index combinations
    outdir_model = f"results/{outfile}/model/"
    check_path(outdir_model)
    len_opt_set, opt_var, status = milp(kits_desired_size, sorted_index_list, matrix, d, outfile)
    #print(len(opt_var), len_opt_set)

    if args.warnings:
        print("Taken indices will be checked for possible collisions..")
        if collison:
            warnings.warn('Be aware that collisions occurred between marked Indices..')
            print(f'The collieded Indexcombinations are: {collided_combis}')
            time.sleep(0.5)


    # Step 6: creating a output file in yaml format, if the milp found optimal variables
    outdir_outyaml = f"results/{outfile}/outyaml/"
    check_path(outdir_outyaml)

    if status == OptimizationStatus.INFEASIBLE:
        exit('Infeasability caused by a higher threshold or bad chosen indices as expected..')
    else:
        yaml_out = f"results/{outfile}/outyaml/{outfile}_out.yaml"
        print(f"Step 6: Final Output Yaml will be saved in {yaml_out}..")
        time.sleep(0.5)
        result, selected = create_outyaml(opt_var[:len_opt_set + 1], d, sorted_index_list, kits_sample_names)
        if not os.path.isfile(yaml_out):
            os.system(f"touch {yaml_out}")
        print(yaml.dump(result), file=open(yaml_out, "w"))

        # Step 7: visualize base content matrices
        print("Step 7: Check if base contents are equally distributed for each index position")
        time.sleep(0.5)
        random.shuffle(selected)
        optimal_variables = index_padding(selected)
        outdir_content = f"results/{outfile}/base_content/"
        check_path(outdir_content)
        base_matrix = create_content_matrix(optimal_variables)
        base_matrix['total'] = pd.Series([0.25, 0.25, 0.25, 0.25], index=base_matrix.index)
        plt.rcParams['figure.dpi'] = 360
        figure(figsize=(15, 9), dpi=80)
        ax = base_matrix.T.plot(kind='bar', stacked=True)
        plt.xlabel(f"{len(optimal_variables)} Barcodes", size=12)
        plt.xticks(rotation=0)
        plt.ylabel("nucleotide %", size=12)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        plt.axhline(y=0.25, color='gray', linestyle='--')
        plt.axhline(y=0.5, color='gray', linestyle='--')
        plt.axhline(y=0.75, color='gray', linestyle='--')
        plt.axhline(y=1, color='gray', linestyle='--')

        sns.despine()

        leg = plt.legend(labels=['A', 'C', 'G', 'T'], bbox_to_anchor=(1.05, 1), loc='upper left',
                         title="Nucleotide Base")
        leg.legendHandles[0].set_color('red')
        leg.legendHandles[1].set_color('green')
        leg.legendHandles[2].set_color('orange')
        leg.legendHandles[3].set_color('blue')
        plt.tight_layout()
        plt.savefig(f'{outdir_content}{outfile}.png')
        #plot_nucleotide_matrix(selected, outfile)

        """"
        Step 8: Web-based graphical profile viewer (Snakeviz)
        generating profile file: python -m cProfile -o program.prof my_program.py
        showing sunburst Chart: snakeviz program.prof
        """
    if args.runtime:
        end_time = time.time()
        runtime = end_time - start_time
        print("CPU-Runtime: ", runtime)


if __name__ == '__main__':
    main()
