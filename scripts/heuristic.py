# imports anpassen
import yaml
import os
import distance
import pandas as pd
import numpy as np
import networkx as nx
from networkx.algorithms.approximation import independent_set, vertex_cover
import igraph as ig
import matplotlib.pyplot as plt
from matplotlib import cm
from pulp import LpMaximize, LpProblem, LpStatus, lpSum, LpVariable
import datetime

# -----------------------------------
# Load Index Adapter from tsv files (OPTIONAL, falls .yml nicht vorhanden)
# gehe komplette file durch, bis "[Indices]" gefunden wird. Lese ab zeile+2 bis zum zeilenumbruch alle sequenzen
# input: tsv file
# output: sequenzen in liste

d1 = {}
directory = r'dataset/all_indexsets'
for entry in os.scandir(directory):
    if entry.path.endswith(".tsv"):
        name = os.path.basename(entry.path)
        name = name.split(".")[0]

        smallist = []
        # print next line after a matching line until break
        copy = False
        with open(entry.path, 'r') as file:
            for line in file:
                if line.startswith("[Indices"):
                    copy = True
                elif line.startswith("[SupportedModules]"):
                    copy = False
                elif copy:
                    temp = line.strip().split()
                    if temp and "Sequence" not in temp:
                        smallist.append(temp[1])
                        d1[name] = smallist

# print(len(d1), d1)


# -----------------------------------
# Get necessary Information from Frontend in yml format

# read yaml in dictionary
with open("frontend.yaml", 'r') as stream:
    try:
        d = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)


# -----------------------------------
# Hilfsfunktion
# get lowest level from dictionary
def recursive_items(dictionary):
    for key_def, value_def in dictionary.items():
        if type(value_def) is dict:
            yield from recursive_items(value_def)
        else:
            yield key_def, value_def


# -----------------------------------
# get index from dictionary
forward_reverse = []
selected = []
index_for_matrix = []
for key, value in d.items():
    if key == "taken_indices":
        for key1, value1 in value.items():
            if "," in key1:
                forward_reverse.append(tuple(key1.split(',')))
                temp = {}
                sample = value1['Samplename']
                temp[sample] = key1
                index_for_matrix.append(temp)
    elif key == "to_select":
        for key2, value2 in recursive_items(value):
            if "," in key2:
                forward_reverse.append(tuple(key2.split(',')))
                selected.append(tuple(key2.split(',')))
                index_for_matrix.append(tuple(key2.split(',')))

print("Alle Tuple Indices: ", forward_reverse)
print("Die zu selektierenden Indices: ", selected)
print("Beides Zusammen für die Matrix: ", index_for_matrix)


# Tupel mit allen Informationen

# -----------------------------------
# Editdistanz als Distanzkoeffizient (Hilfsfunktion)
# betrachtet forward und reverse als tuple
# Levenshtein best common method while its allow edit operations on strings with different length
# It is calculated as the minimum number of single-character edits necessary to
#     transform one string into another.
# leere Einträge werden nicht gewertet durch max()
def dist(tuple1, tuple2):
    if isinstance(tuple1, tuple) and isinstance(tuple2, tuple):
        difference = max(distance.levenshtein(tuple1[0], tuple2[0]), distance.levenshtein(tuple1[1], tuple2[1]))
    elif isinstance(tuple1, dict) and isinstance(tuple2, dict):
        tuple1 = [tuple(value.split(',')) for key, value in tuple1.items()][0]
        tuple2 = [tuple(value.split(',')) for key, value in tuple2.items()][0]
        difference = dist(tuple1, tuple2)
    elif isinstance(tuple1, dict) or isinstance(tuple2, tuple):
        tuple1 = [tuple(value.split(',')) for key, value in tuple1.items()][0]
        difference = dist(tuple1, tuple2)
    elif isinstance(tuple2, dict) or isinstance(tuple1, tuple):
        tuple2 = [tuple(value.split(',')) for key, value in tuple2.items()][0]
        difference = dist(tuple1, tuple2)
    elif isinstance(tuple1, str):  # nur für tsv files
        difference = distance.levenshtein(tuple1, tuple2)
    return difference


quit()


# -----------------------------------
# Weise einem Tupel seinem Kit zu. (Hilfsfunktion)
def get_kit_from_index(index):
    from_set = None
    test = None
    if isinstance(index, dict):
        for key, value in index.items():
            test = value
    else:
        test = ','.join(index)
    for key, value in d.items():
        if isinstance(value, dict):
            for key1, value1 in value.items():
                if test in key1:
                    from_set = value1['from_set']
                else:
                    for key2, value2 in value1.items():
                        if isinstance(value2, dict):
                            if test in value2:
                                from_set = key2
    return from_set


# -----------------------------------
# anzahl an indices pro set in form eines dictonaries
# anhand dieser Dictonary, lässt sich die Matrix in Anzahl der Zeile pro Kit trennen

def get_kit(allindices):
    kits = {}
    for i in allindices:
        if get_kit_from_index(i) in kits:
            kits[get_kit_from_index(i)] += 1
        else:
            kits[get_kit_from_index(i)] = 1
    return kits


kits = get_kit(forward_reverse)

# NACH SETS SORTIEREN
sorted_index_list = []
for i in kits:
    [sorted_index_list.append(j) for j in index_for_matrix if get_kit_from_index(j) == i]
print("Die sortiere Liste für die Matrix lautet: ", sorted_index_list)


# -----------------------------------
# Compute distance matrix
# input: list of sequences
# output: distance matrix
def distance_matrix(sequences):
    n = len(sequences)
    my_array = np.zeros((n, n))
    for i, ele_1 in enumerate(sequences):
        for j, ele_2 in enumerate(sequences):
            if j >= i:
                break
            difference = dist(ele_1, ele_2)  # Levenshtein Distance
            my_array[i, j] = difference
            my_array[j, i] = difference
    return my_array


threshold = d["dist_threshold"]


# -----------------------------------
# Set a threshold in difference matrix to a minimum distance constrained, so the
# pairwise base-distance is maximal: 2 (variable)
# otherwise the cell value will be assigned 0
def compute_matrix(sequences):
    matrix = distance_matrix(sequences)
    temp = [str(i) for i in sequences]
    dfs = pd.DataFrame(matrix, index=temp, columns=temp)
    dfs[dfs <= threshold] = 0
    dfs[dfs > 0] = 1  # binäre darstellung, sprich alle anderen Einträge 1? könnte hilfreich beim ilp werden
    return dfs


matrix = compute_matrix(sorted_index_list)
print(f"Distanzmatrix mit Threshold {threshold}: ", "\n", matrix)
print("\n", kits)
for i in index_for_matrix:
    print(i, ":", get_kit_from_index(i))
quit()
'''
# ---------------------------------
# count time start
# begin_time = datetime.datetime.now()  # cpu zeit

# Graph from difference matrix using NetworX (heuristic) #muss optimiert werden
def matrix_to_graph(matrix):
    G = nx.Graph(matrix)
    nx.draw(G, edge_color=[i[2]['weight'] for i in G.edges(data=True)], edge_cmap=cm.winter)
    G.edges(data=True)
    # plt.show()
    return G


graph1 = matrix_to_graph(df_nextera)
# graph2 = matrix_to_graph(df_nex_trus)
# graph3 = matrix_to_graph(df_nex_trud)
# graph4 = matrix_to_graph(df_nex_tru)
# graph5 = matrix_to_graph(df_tru_s_d) #kleinster Graph
# graph6 = matrix_to_graph(df_tru_s_tru)
# graph7 = matrix_to_graph(df_tru_d_tru)


# Grade der Knoten
# degrees = [(node, val) for (node, val) in graph.degree()]
# print("Degrees of Nodes in G: ", degrees)

marked_indices = []
for key, value in d.items():
    if key == "taken_indices":
        for key1, value1 in value.items():
            if "," in key1:
                marked_indices.append(tuple(key1.split(',')))

# maximal independent set
# A maximal independent set is an independent set such that it is not possible to add a new node and still get an independent set.
# maximal2 = nx.maximal_independent_set(graph2, marked_indices)
# It is possible to assign nodes that must be part of the independent set. This set of nodes must be independent.
# If the nodes in the provided list are not part of the graph or do not form an independent set, an exception is raised.



# maximum independent set
# maximum1 = independent_set.maximum_independent_set(graph1) # 1:55, |set| = 1
# maximum2 = independent_set.maximum_independent_set(graph2)  # 1:56, |set| = 2
# maximum3 = independent_set.maximum_independent_set(graph3) # 1:56, |set| = 2
# maximum4 = independent_set.maximum_independent_set(graph4) # 3:32, |set| = 2
# maximum5 = independent_set.maximum_independent_set(graph5) # 0:016, |set| = 2
# maximum6 = independent_set.maximum_independent_set(graph6) # 0:157, |set| = 2
# maximum7 = independent_set.maximum_independent_set(graph7) # 0:02, |set| = 2
# print("Maximum Independent Set: ", maximum1)
# print("Unabhängigkeitszahl: ", len(maximum1))
# print("Maximum Independent Set: ", maximum2)
# print("Unabhängigkeitszahl: ", len(maximum2))
# print("Maximum Independent Set: ", maximum3)
# print("Unabhängigkeitszahl: ", len(maximum3))
# print("Maximum Independent Set: ", maximum4)
# print("Unabhängigkeitszahl: ", len(maximum4))
# print("Maximum Independent Set: ", maximum5)
# print("Unabhängigkeitszahl: ", len(maximum5))
# print("Maximum Independent Set: ", maximum6)
# print("Unabhängigkeitszahl: ", len(maximum6))
# print("Maximum Independent Set: ", maximum7)
# print("Unabhängigkeitszahl: ", len(maximum7))

# print(datetime.datetime.now() - begin_time)

# ---------------------------------
# Graph from difference matrix using iGraph (exact algorithm)
# https://igraph.org/python/doc/python-igraph.pdf
# dflist = [df_nextera, df_nex_trus, df_nex_trud, df_nex_tru, df_tru_s_d, df_tru_s_tru, df_tru_d_tru]
g = ig.Graph.DataFrame(df_nextera, directed=False)

out_name = "results/graphs/tru_d_tru.png"
layout = g.layout("fruchterman_reingold")
ig.plot(g, layout=layout)

# print("Number of vertices in the graph:", g.vcount())
# print("Number of edges in the graph", g.ecount())
# print("Is the graph directed:", g.is_directed())
# print("Maximum degree in the graph:", g.maxdegree())
# print("Adjacency matrix:\n", g.get_adjacency())
print("Independent Number of Graph: ", g.independence_number())
print("Some Independent Vertex Sets ", g.independent_vertex_sets(min=2))
print("Maximal Independent Sets ", g.maximal_independent_vertex_sets())
print("Maximum Independent Sets ", g.largest_independent_vertex_sets())

'''

# ---------------------------------
# ILP Formulierung in PulP

# Define the model
model = LpProblem(name="maximal-independent-set", sense=LpMaximize)
solver = pulp.getSolver('CPl')

# Initialize the decision variables
x = {i: LpVariable(name=f"x{i}", lowBound=0) for i in range(1, n)}

M = 100  # anzahl an desired samplenames aus der yaml

# Add constraints
# model += [(lpSum(x.values()) <= y[i] * n, f"Set{i}_constraint" for i in
#        range(1, m))]  # so viele werden aus einem Set genommen
model += (lpSum(x.values()) == M)  # so viele werden insgesamt aus allen Sets selected
# threshold muss erfüllt sein
model += x[i] + x[j] <= 1  # eine von beiden indices wird ausgewählt
# Solve the optimization problem
status = model.solve()

# print the model
print(f"status: {model.status}, {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")

for var in model.variables():
    print(f"{var.name}: {var.value()}")

for name, constraint in model.constraints.items():
    print(f"{name}: {constraint.value()}")

# output aus ilp: anzahl an indices pro set, jedes constraint set wird einer optimalen anzahl zugewiesen

# indices werden den sets zugewiesen, sodass to_select: kits: ...: desired_samplenames aufgefüllt wird


# ---------------------------------
# datenstruktur der ausgabe wird ein dictonary sein in der Form
# desired_samplenames = {LI-2020-2: null, EC-2020-6: null, LI-2020-1: null, EC-2020-5: null}
