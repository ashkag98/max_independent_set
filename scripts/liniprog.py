'''
# Using PuLP - more convinient than Scipy
from pulp import LpMaximize, LpProblem, LpStatus, lpSum, LpVariable

# Define the model
model = LpProblem(name="resource-allocation", sense=LpMaximize)

# Define the decision variables
# Decision Variables: Sets
# cat = "Constantly" or "Integer" or "Boolean"
x = {i: LpVariable(name=f"x{i}", lowBound=0) for i in range(1, 5)}
y = {i: LpVariable(name=f"x{i}", lowBound=0, cat="Binary") for i in (2,3,4)}

# Add constraints
model += (lpSum(x.values()) <= 50, "manpower")
model += (3 * x[1] + 2 * x[2] + x[3] <= 100, "material_a")
model += (x[2] + 2 * x[3] + 3 * x[4] <= 90, "material_b")

# Set the objective
model += 20 * x[1] + 12 * x[2] + 40 * x[3] + 25 * x[4]

# Solve the optimization problem
status = model.solve()

# Get the results
print(f"status: {model.status}, {LpStatus[model.status]}")
print(f"objective: {model.objective.value()}")

for var in x.values():
    print(f"{var.name}: {var.value()}")

for name, constraint in model.constraints.items():
    print(f"{name}: {constraint.value()}")



# Problem: Maximal Independent Sets
model = LpProblem(name="Independent Set", sense=LpMaximize)

# Define the decision variables
# Decision Variables: Sets
# Continous or Integer?
x = {i: LpVariable(name=f"x{i}", lowBound=0) for i in range(1, 5)} #je nachdem, wie viele Decision Variables man braucht

index_preped = 10

# Add constraints
model += (lpSum(x.values()) <= 50, "Indices im Set") # je nachdem, wie viele Indices später im Set enthalten sein sollen
model += (x[2] + x[3] + x[4] <= 40) #Preped indices aus set a sind bereits enthalten

# Set the objective
model += 20 * x[1] + 12 * x[2] + 40 * x[3] + 25 * x[4] #je nachdem, wie viele Indices aus welchen Sets genommen werden
'''

#Guropi - schnellster ILP solver
from gurobipy import *
R = ['Carlos', 'Joe', 'Monika']
J = ['Tester', 'JavaDeveloper', 'Architect']

combinations, ms = multidict({
    ('Carlos', 'Tester'): 53,
    ('Carlos', 'JavaDevolper'): 27,
    ('Carlos', 'Architect'): 13,
    ('Joe', 'Tester'): 80,
    ('Joe', 'JavaDeveloper'): 47,
    ('Joe', 'Architect'): 67,
    ('Monika', 'Tester'): 53,
    ('Monika', 'JavaDeveloper'): 73,
    ('Monika', 'Architect'): 47
})

m = Model('RAP') #resource assignment problem

#create decision variables for the RAP model
x = m.addVars(combinations, name="assign")

#create jobs constraints
jobs = m.addConstrs((x.sum('*',j) == 1 for j in J), 'job')

#create resource constraints
resources = m.addConstrs((x.sum(r, '*') <= 1 for r in R), 'resource')

#the objective is to maximize total matching score of assignments
m.setObjective(x.prod(ms), GRB.MAXIMIZE)

#save model for inspection
m.write('RAP.lp')

#run optimization enginge, that solve the lp problem
m.optimize()

#display optimal values of decision variables
for v in m.getVars():
    if (abs(v.x) > 1e-6):
        print(v.varName, v.x)

#display optimal total matching score
print('total matching scores', m.objVal)


# With gurobi
# ---------------------------------
# ILP Formulierung
# Guropi - schnellster ILP solver
from gurobipy import *
from mip import *

# Hier kommt das, was vor der ILP Formulierung festgelegt wird:
# ausgewählte Indices, die geprept wurden (markierte Indices)
index_list = []
# Anzahl an Sequencen, die zu beobachten sind
max_index = 0

# (M)ILP
# m = Model('MIS')  # maximal independent set problem
m = Model('MIS', sense=MAXIMIZE, solver_name=CBC)

# create decision variables for the RAP model
# decision variables sind nicht negativ
x = m.addVars(name="setA")  # integer
# x = m.addVars(name="setB")  # integer
# x = m.addVars(name="setC")  # integer

n = nextera_seq
y = [m.add_var(var_type=BINARY) for i in range(n)]
# y = m.addVars(index_list, name="assign")  # binary

# create constraints:
# (1) keine Distanz darf > threshold sein
# (2) x Werte aus dem SetA, y Werte aus Set B und z Werte aus Set C
# (3) Set von definierten (markierte) Indices soll in der Auswahl enhalten sein
# (4) markierte Indices sollen aus (2) enfernt werden
# (5) maximale Anzahl zu untersuchender Indices
sets = m.addConstrs((x.sum('*', j) == 1 for j in index_list), 'set')  # constraint für "assign" y
# x_1 + x_2 + x_3 <= y_a * 3 -> x sind SetA variablen, x_i sind die seqs
resources = m.addConstrs((x.sum(r, '*') <= 1 for r in index_list), 'resource')
# pro Set 1 constraint?

# the objective is to maximize total matching score of assignments:
# maximum aller paarweisen distanzen aus allen Sets
# Überlegung: max aus allen y Summen
m.setObjective(x.prod(y), GRB.MAXIMIZE)

# save model for inspection
m.write('RAP.lp')

# run optimization engine, that solve the lp problem
m.optimize()


# display optimal values of decision variables
def print_solution(model):
    for var in model.getVars():
        if abs(var.x) > 1e-6:
            print("{0}: {1}".format(var.varName, var.x))
    print('Total matching score: {0}'.format(model.objVal))
    return None


print_solution(m)